package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author jack
 */
@Data
@TableName("t_test_date")
public class TestDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    private LocalTime time;

    private LocalDate date;

    private LocalDateTime datetime;

    private LocalTime localtime;

    private LocalDate localdate;

    private LocalDateTime localdatetime;
}
