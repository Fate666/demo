package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @author jack
 */
@Slf4j
@ComponentScan(basePackages = "com")
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Value("${spring.application.name}")
    private String name;

    @Value("${server.port}")
    private String port;

    @Value("${spring.profiles.active}")
    private String env;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("【启动成功】 应用名称：「{}」,端口「{}」,当前环境「{}」", name, port, env);
    }
}
