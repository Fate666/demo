package com.example.demo.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.demo.entity.TestDate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import com.example.demo.service.TestDateService;

import java.sql.Wrapper;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jack
 * @since 2023-04-09 21:15:20
 */
@RestController
@RequestMapping("/test")
public class TestDateController {

    @Resource
    private TestDateService testDateService;

    @PostMapping("/list")
    public List<TestDate> list() {
        return testDateService.list(Wrappers.lambdaQuery(TestDate.class)
                .in(TestDate::getId, 1,2,3,4,5));
    }
}
