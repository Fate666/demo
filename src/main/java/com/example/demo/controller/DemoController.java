package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author jack
 */
@Slf4j
@EnableScheduling
@RestController
@RequestMapping(value = "/demo")
public class DemoController {

    @GetMapping("/hello")
    public String hello(String str) {
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS"));
        log.info("was visited,dateTime:{},str:{}", dateTime, str);
        return String.format("hello,%s\t\t%s", str, dateTime);
    }

    /**
     * 测试定时任务
     * 每隔1小时执行一次
     */
    @GetMapping("/testJob")
    @Scheduled(cron = "0 0 */1 * * ?")
    public String testJob() {
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS"));
        log.info("Execute at {}" + dateTime);
        return "Execute at " + dateTime;
    }
}
