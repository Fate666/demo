package com.example.demo.generator;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

import java.sql.Types;
import java.util.Collections;

/**
 * @author jack
 */
public class MybatisPlusGenerator {

    private static final String URL = "jdbc:mysql://192.168.28.200:3306/test?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";

    private static final String USERNAME = "root";

    private static final String PASSWORD = "123456";

    private static final String PARENT_PACKAGE_NAME = "com.example.demo";

    private static final String MODULE_NAME = "demo";

    private static final String[] TABLE_NAMES = {"t_test_date"};

    private static final String TABLE_PREFIX = "t_";

    private static final String AUTHOR = "jack";

    public static void main(String[] args) {
        // 获取当前路径
        String projectPath = System.getProperty("user.dir");

        // 自动填充配置
        Column delFlag = new Column("del_flag", FieldFill.INSERT);
        Column createByName = new Column("create_by_name", FieldFill.INSERT);
        Column createTime = new Column("create_time", FieldFill.INSERT);
        Column updateByName = new Column("update_by_name", FieldFill.UPDATE);
        Column updateTime = new Column("update_time", FieldFill.UPDATE);

        // 生成器
        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author(AUTHOR)
                            .outputDir(projectPath + "/src/main/java")
                            // .enableSwagger()
                            .commentDate("yyyy-MM-dd HH:mm:ss")
                            .dateType(DateType.TIME_PACK)
                            .build();
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(PARENT_PACKAGE_NAME)
                            // .moduleName(MODULE_NAME)
                            .entity("entity")
                            .service("service")
                            .serviceImpl("service.impl")
                            .mapper("dao")
                            .xml("dao.mapper")
                            .controller("controller")
                            .build();
                })
                .templateEngine(new VelocityTemplateEngine())
                .templateConfig(builder -> {
                    builder.entity("/templates/entity.java.vm")
                            .mapper("/templates/mapper.java.vm")
                            .xml("/templates/mapper.xml.vm")
                            .service("/templates/service.java.vm")
                            .serviceImpl("/templates/serviceImpl.java.vm")
                            .controller("/templates/controller.java.vm");
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder.enableSkipView()
                            .disableSqlFilter()
                            .addInclude(TABLE_NAMES)
                            .addTablePrefix(TABLE_PREFIX)
                            .entityBuilder()
                            .enableChainModel()
                            .enableLombok()
                            .enableRemoveIsPrefix()
                            .enableTableFieldAnnotation()
                            .addTableFills(delFlag, createByName, createTime, updateByName, updateTime)
                            .versionColumnName("version")
                            .versionPropertyName("version")
                            //.logicDeleteColumnName("del_status")
                            //.logicDeletePropertyName("del_status")
                            .naming(NamingStrategy.underline_to_camel)
                            // 用户自己输入id
                            .idType(IdType.ASSIGN_ID)
                            //.disableSerialVersionUID()
                            .controllerBuilder()
                            .enableRestStyle()
                            //.enableHyphenStyle()
                            .serviceBuilder()
                            // 保留接口名中的I风格
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImpl")
                            .mapperBuilder()
                            .enableMapperAnnotation()
                            .enableBaseResultMap()
                            .enableBaseColumnList()
                            .formatMapperFileName("%sMapper")
                            .formatXmlFileName("%sMapper");
                }).execute();
    }
}
