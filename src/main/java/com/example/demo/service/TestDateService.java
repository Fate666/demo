package com.example.demo.service;

import com.example.demo.entity.TestDate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jack
 * @since 2023-04-09 21:15:20
 */
public interface TestDateService extends IService<TestDate> {

}
