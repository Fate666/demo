package com.example.demo.service.impl;

import com.example.demo.entity.TestDate;
import com.example.demo.dao.TestDateMapper;
import com.example.demo.service.TestDateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jack
 * @since 2023-04-09 21:15:20
 */
@Service
public class TestDateServiceImpl extends ServiceImpl<TestDateMapper, TestDate> implements TestDateService {

}
