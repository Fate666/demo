package com.example.demo.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author jack
 */
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Object createBy = getFieldValByName("createByName", metaObject);
        if (Objects.isNull(createBy)) {
            setFieldValByName("createByName", "system", metaObject);
        }

        Object createTime = getFieldValByName("createTime", metaObject);
        if (Objects.isNull(createTime)) {
            setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        }

        Object delFlag = getFieldValByName("delFlag", metaObject);
        if (Objects.isNull(delFlag)) {
            setFieldValByName("delFlag", 0, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object updateBy = getFieldValByName("updateByName", metaObject);
        if (Objects.isNull(updateBy)) {
            setFieldValByName("updateByName", "system", metaObject);
        }

        Object updateTime = getFieldValByName("updateTime", metaObject);
        if (Objects.isNull(updateTime)) {
            setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        }
    }
}
