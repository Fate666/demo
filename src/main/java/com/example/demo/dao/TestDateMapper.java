package com.example.demo.dao;

import com.example.demo.entity.TestDate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jack
 * @since 2023-04-09 21:15:20
 */
@Mapper
public interface TestDateMapper extends BaseMapper<TestDate> {

}
